import React, { Component } from 'react';

class SignUp extends Component {
    render() {
        return (
            <div className="modal fadeInUp probootstrap-animated" id="signupModal" tabIndex={-1} role="dialog" aria-labelledby="signupModalLabel" aria-hidden="true">
                        <div className="vertical-alignment-helper">
                            <div className="modal-dialog modal-md vertical-align-center">
                                <div className="modal-content">
                                    <button type="button" className="close" data-dismiss="modal" aria-hidden="true"><i className="icon-cross" /></button>
                                    <div className="probootstrap-modal-flex">
                                        <div className="probootstrap-modal-content">
                                            <form action="#" className="probootstrap-form">
                                                <div className="form-group">
                                                    <input  type="text" name="fName" className="form-control" placeholder="Họ tên của bạn" />
                                                </div>
                                                <div className="form-group">
                                                    <input type="text"  name="fPhone" className="form-control" placeholder="Số điện thoại của bạn" />
                                                </div>
                                                <div className="form-group">
                                                    <input type="text"name="fAge" className="form-control" placeholder="Tuổi của bạn" />
                                                </div>
                                                <div className="form-group">
                                                    <input type="text" name="fNeed" className="form-control" placeholder="Nhu cầu học" />
                                                </div>
                                                <div className="form-group">
                                                    <input type="text" name="fLevel" className="form-control" placeholder="Trình độ hiện tại" />
                                                </div>
                                                <div className="form-group text-left">
                                                    <div className="row">
                                                        <div className="col-md-6">
                                                            <input type="submit" className="btn btn-primary btn-block" defaultValue="Đăng Ký" style={{ width: '100%', marginTop: '30px' }} />
                                                        </div>
                                                    </div>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
        );
    }
}

export default SignUp;