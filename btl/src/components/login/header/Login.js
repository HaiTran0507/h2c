import React, { Component } from 'react';
import { LoginAc } from '../../../action/LoginAc';
import { Redirect } from 'react-router-dom'

class Login extends Component {
    constructor(props) {
        super(props);
        this.state = {
            redirect: false,
            username: '',
            password: ''
            
        }
        this.SubmitForm = this.SubmitForm.bind(this);
        this.onChange = this.onChange.bind(this);
    }
    SubmitForm()  {
        if (this.state.username && this.state.password) {
            LoginAc(this.state).then((result) => {
                let reponseJson = result;
                if (reponseJson.accessToken) {
                    console.log(reponseJson);
                    
                    sessionStorage.setItem('accessTokename', reponseJson.accessToken);
                    this.setState({ redirect: true });
                } else {
                    alert("Đăng nhập thất bại")
                }
            });
        }

    }
    onChange (event){
        this.setState({
            [event.target.name]: event.target.value
        });
    }

    render() {
        if (this.state.redirect) {
            return <Redirect to={'/student-page'}/>
        }

        // if (sessionStorage.getItem("accessTokename")) {
        //     return (<Redirect to={'/student-page'} />)
        // }
        return (
            <div className="modal fadeInUp probootstrap-animated" id="loginModal" tabIndex={-1} role="dialog" aria-labelledby="loginModalLabel" aria-hidden="true">
                <div className="vertical-alignment-helper">
                    <div className="modal-dialog modal-md vertical-align-center">
                        <div className="modal-content">
                            <button type="button" className="close" data-dismiss="modal" aria-hidden="true"><i className="icon-cross" /></button>
                            <div className="probootstrap-modal-flex">
                                <div className="probootstrap-modal-content">
                                    <div  className="probootstrap-form">
                                        <div className="form-group">
                                            <input type="text" name="username" className="form-control" placeholder="Tên đăng nhập" value={this.state.userName} onChange={this.onChange} />
                                        </div>
                                        <div className="form-group">
                                            <input name="password" type="password" className="form-control" placeholder="Mật khẩu" value={this.state.password} onChange={this.onChange} />
                                        </div>
                                        <div className="form-group clearfix mb40">
                                            <a href="#" className="probootstrap-forgot">[Quên mật khẩu]</a>
                                        </div>
                                        <div className="form-group text-left">
                                            <div className="row">
                                                <div className="col-md-6">
                                                    <input onClick={this.SubmitForm} type="submit" className="btn btn-primary btn-block" value="Đăng nhập" style={{ width: '139%',fontSize: '16px'}} />
                                                </div>

                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

export default Login;