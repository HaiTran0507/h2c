import React, { Component } from 'react';
import Login from './Login';
import SignUp from './SignUp';

class Header extends Component {
    render() {
        return (
            <div>
                <nav className="navbar-default navbar-fixed-top probootstrap-navbar" style={{position: 'fixed'}}>
                    <div className="container" style={{display: 'block'}}>
                        <div className="navbar-header">
                            <button type="button" className="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-collapse" aria-expanded="false" aria-controls="navbar">
                                <span className="sr-only">Toggle navigation</span>
                                <span className="icon-bar" />
                                <span className="icon-bar" />
                                <span className="icon-bar" />
                            </button>
                            <a className="navbar-brand" href="index.html" title="uiCookies:Stack">Stack</a>
                        </div>
                        <div id="navbar-collapse" className="navbar-collapse collapse">
                            <ul className="nav navbar-nav navbar-right" style={{display: 'block'}}>
                                <li><a href="index.html">Trang chủ</a></li>
                                <li><a href="index.html">Các khoá học</a></li>
                                <li><a href="contact.html">Liên lạc</a></li>
                                <li className="probootstra-cta-button"><a className="btn" data-toggle="modal" data-target="#loginModal">Đăng Nhập</a></li>
                                <li className="probootstra-cta-button last"><a  className="btn btn-ghost" data-toggle="modal" data-target="#signupModal">Đăng Ký</a></li>
                            </ul>
                        </div>
                    </div>
                </nav>
                <div>
                    <Login />
                    <SignUp />
                </div>
            </div>
            // <div id="right-panel" className="right-panel">
            //     {/* Header*/}
            //     <header id="header" className="header">
            //         <div className="top-left">
            //             <div className="navbar-header">

            //             </div>
            //         </div>
            //         <div className="top-right">
            //             <div className="header-menu">
            //                 <div className="header-left">

            //                     <div className="user-area dropdown float-right">
            //                         <ul className="nav navbar-nav navbar-right">
            //                             <li><a href="index.html">Trang chủ</a></li>
            //                             <li><a href="index.html">Các khoá học</a></li>
            //                             <li><a href="contact.html">Liên lạc</a></li>
            //                             <li className="probootstra-cta-button"><a href="xxx" className="btn" data-toggle="modal" data-target="#loginModal">Đăng Nhập</a></li>
            //                             <li className="probootstra-cta-button last"><a href="xxx" className="btn btn-ghost" data-toggle="modal" data-target="#signupModal">Đăng Ký</a></li>
            //                         </ul>

            //                     </div>
            //                 </div>
            //             </div>
            //         </div></header>
            //     {/* /#header */}
            // </div>

        );
    }
}

export default Header;
