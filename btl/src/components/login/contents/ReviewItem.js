import React, { Component } from 'react';

class ReviewItem extends Component {
    render() {
        return (
            <li>
                <i className="fa fa-check fa-2x" aria-hidden="true" style={{ color: 'orangered' }} />
                {this.props.description}
            </li>
        );
    }
}

export default ReviewItem;