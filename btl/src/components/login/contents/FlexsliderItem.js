import React, { Component } from 'react';

class FlexsliderItem extends Component {
    render() {
        return (
            <li style={{backgroundImage: this.props.style }} className="overlay">
                <div className="container">
                    <div className="row">
                        <div className="col-md-8 col-md-offset-2">
                            <div className="probootstrap-slider-text text-center">
                                <h1 className="probootstrap-heading">{this.props.description}</h1>
                            </div>
                        </div>
                    </div>
                </div>
            </li>
        );
    }
}

export default FlexsliderItem;