import React, { Component } from 'react';
import data from './../../../data/feedBack.json';
import FeedBackItem from './FeedBackItem'

class FeedBack extends Component {
    render() {
        return (
            <div>
                <section className="probootstrap-section probootstrap-border-top probootstrap-bg-white">
                    <div className="container">
                        <div className="row">
                            <div className="col-md-6 col-md-offset-3 text-center section-heading probootstrap-animate">
                                <h2>Cảm nhận từ học viên</h2>
                            </div>
                        </div>
                        {/* END row */}
                        <div className="row">
                            <div className="col-md-12">
                                <div className="owl-carousel owl-carousel-fullwidth">
                                    {
                                        data.map((value, key) => {
                                            return (
                                                <FeedBackItem key={key}
                                                    img={value.img}
                                                    description={value.description}></FeedBackItem>
                                            )
                                        })
                                    }
                                </div>
                            </div>
                        </div>
                        {/* END row */}
                    </div>
                </section>

            </div>
        );
    }
}

export default FeedBack;