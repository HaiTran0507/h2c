import React, { Component } from 'react';

class Showcase extends Component {
    render() {
        return (
            <div>
                <section className="probootstrap-section proboostrap-clients probootstrap-bg-white probootstrap-zindex-above-showcase">
                    <div className="container">
                        <div className="row">
                            <div className="col-md-6 col-md-offset-3 text-center section-heading probootstrap-animate">
                                <h2>Big Company Trusts Us</h2>
                                <p className="lead">Lorem ipsum dolor sit amet consectetur adipisicing elit. Iusto provident qui tempore natus quos quibusdam soluta at.</p>
                            </div>
                        </div>
                        {/* END row */}
                        <div className="row">
                            <div className="col-md-3 col-sm-6 col-xs-6 text-center client-logo probootstrap-animate" data-animate-effect="fadeIn">
                                <img src="img/client_1.png" className="img-responsive" alt="Free Bootstrap Template by uicookies.com" />
                                <b className="count">150</b>
                                <span>giảng viên</span>
                            </div>
                            <div className="col-md-3 col-sm-6 col-xs-6 text-center client-logo probootstrap-animate" data-animate-effect="fadeIn">
                                <img src="img/client_2.png" className="img-responsive" alt="Free Bootstrap Template by uicookies.com" />
                                <b className="count">150</b>
                                <span>giảng viên</span>
                            </div>
                            <div className="clearfix visible-sm-block visible-xs-block" />
                            <div className="col-md-3 col-sm-6 col-xs-6 text-center client-logo probootstrap-animate" data-animate-effect="fadeIn">
                                <img src="img/client_3.png" className="img-responsive" alt="Free Bootstrap Template by uicookies.com" />
                                <b className="count">150</b>
                                <span>giảng viên</span>
                            </div>
                            <div className="col-md-3 col-sm-6 col-xs-6 text-center client-logo probootstrap-animate" data-animate-effect="fadeIn">
                                <img src="img/client_4.png" className="img-responsive" alt="Free Bootstrap Template by uicookies.com" />
                                <b className="count">150</b>
                                <span>giảng viên</span>
                            </div>
                        </div>
                    </div>
                </section>

            </div>
        );
    }
}

export default Showcase;