import React, { Component } from 'react';
import CourseItem from './CourseItem';
import data from './../../../data/courses.json'
class Course extends Component {
    render() {

        return (
            <div>
                <section className="probootstrap-section probootstrap-bg-white probootstrap-zindex-above-showcase">
                    <div className="container">
                        <div className="row">
                            <div className="col-md-6 col-md-offset-3 text-center section-heading probootstrap-animate" data-animate-effect="fadeIn">
                                <h2>Các khoá học cho người mới bắt đầu</h2>
                            </div>
                        </div>
                        <div className="row probootstrap-gutter60">
                            
                            {
                                data.map((value, key) => {
                                    return (
                                        <CourseItem key={key}
                                            title={value.title}
                                            description={value.description}></CourseItem>
                                    )
                                })
                            }
                        </div>

                        <div className="row mt50">
                            <div className="col-md-12 text-center">
                                <a href="services.html" className="btn btn-primary btn-lg" role="button">Tất cả các dịch vụ</a>
                            </div>
                        </div>
                    </div>
                </section>

            </div>
        );
    }
}

export default Course;