import React, { Component } from 'react';

class FeedBackItem extends Component {
    render() {
        return (
            <div className="item">
                <div className="probootstrap-testimony-wrap text-center">
                    <figure>
                        <img src={this.props.img} alt="Free Bootstrap Template by uicookies.com" />
                    </figure>
                    <blockquote className="quote">{this.props.description}</blockquote>
                </div>
            </div>
        );
    }
}

export default FeedBackItem;