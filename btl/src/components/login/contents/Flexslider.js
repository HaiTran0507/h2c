import React, { Component } from 'react';
import FlexsliderItem from './FlexsliderItem';
import data from './../../../data/flexslider.json';
class Flexslider extends Component {
    render() {
        return (
            <div>
                <section className="flexslider">
                    <ul className="slides">
                        {
                            data.map((value, key) => {
                                return (
                                    <FlexsliderItem key={key}
                                        style={value.style}
                                        description={value.description}></FlexsliderItem>
                                )
                            })
                        }

                    </ul>
                </section>

            </div>
        );
    }
}

export default Flexslider;