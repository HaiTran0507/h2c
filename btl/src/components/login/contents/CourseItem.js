import React, { Component } from 'react';

class CourseItem extends Component {
    render() {
        return (
                <div className="col-md-4 probootstrap-animate" data-animate-effect="fadeIn">
                    <div className="service hover_service text-center">
                        <div className="icon"><i className="icon-mobile3" /></div>
                        <div className="text">
                            <h3>{this.props.title}</h3>
                            <p>{this.props.description}</p>
                        </div>
                    </div>
                </div>
        );
    }
}

export default CourseItem;