import React, { Component } from 'react';
import ReviewItem from './ReviewItem';
import data from './../../../data/review.json';

class FeedbackStudent extends Component {
    render() {
        return (
            <div>
                <section className="course-review" style={{ margin: '0 8% 0 8%' }}>
                    <h1 className="preview" style={{ textAlign: 'center' }}>Sự khác biệt trong chương trình học của H2C là gì?</h1>
                    <div className="feedback-student" style={{ marginTop: '40px' }}>
                        <div className="row">
                            <div className="col-md-6">
                                <ul className="feedback-text" style={{ listStyle: 'none' }}>
                                    {
                                        data.map((value, key) => {
                                            return (
                                                <ReviewItem key={key} 
                                                    description={value.description}></ReviewItem>
                                            )
                                        })
                                    }
                                </ul>
                            </div>
                            <div className="col-md-6" style={{ textAlign: 'right' }}>
                                <img src="img/review1.jpg" alt="" style={{ width: '95%' }} />
                            </div>
                        </div>
                    </div>
                </section>

            </div>
        );
    }
}

export default FeedbackStudent;