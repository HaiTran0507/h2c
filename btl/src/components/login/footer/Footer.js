import React, { Component } from 'react';

class Footer extends Component {
    render() {
        return (
            <div>
                <footer className="probootstrap-footer">
                    <div className="container">
                        <div className="row">
                            <div className="col-md-6">
                                <div className="probootstrap-footer-widget">
                                    <h3>Paragraph</h3>
                                    <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Iusto provident qui tempore natus quos quibusdam soluta at.</p>
                                    <ul className="probootstrap-footer-social">
                                        <li><a href="/"><i className="icon-twitter" /></a></li>
                                        <li><a href="/"><i className="icon-facebook" /></a></li>
                                        <li><a href="/"><i className="icon-youtube" /></a></li>
                                    </ul>
                                </div>
                            </div>
                            <div className="col-md-6">
                                <div className="row">
                                    <div className="col-md-4">
                                        <div className="probootstrap-footer-widget">
                                            <h3>Thông tin</h3>
                                            <ul>
                                                <li><a href="/">Địa chỉ: </a></li>
                                                <li><a href="/">Điện thoại liên hệ: </a></li>
                                                <li><a href="/">Điều khoản dịch vụ:</a></li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </footer>
                <div></div>
                    
            </div>


        );
    }
}

export default Footer;