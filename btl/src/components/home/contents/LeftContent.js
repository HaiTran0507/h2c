import React, { Component } from 'react';

class LeftContent extends Component {
    render() {
        return (
            <aside id="left-panel" className="left-panel">
  <nav className="navbar navbar-expand-sm navbar-default">
    <div id="main-menu" className="main-menu collapse navbar-collapse">
      <ul className="nav navbar-nav">
        <li className="menu-title">Khóa học của tôi</li>
        <li className="menu-item-has-children dropdown">
          <a href="#" className="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <i className="menu-icon fa fa-tasks" />Khóa học của tôi</a>
          <ul className="sub-menu children dropdown-menu">
            <li><i className="fa fa-circle-o" /><a href="font-fontawesome.html">Xem thông tin khóa học</a></li>
            <li><i className="fa fa-circle-o" /><a href="font-themify.html">Tài liệu</a></li>
          </ul>
        </li>
      </ul>
    </div>{/* /.navbar-collapse */}
  </nav>
</aside>

        );
    }
}

export default LeftContent;