import React, { Component } from 'react';

class HeaderHome extends Component {
    render() {
        return (
            <header id="header" className="header" style={{width: '72%'},{backgroundColor: '#c4f5f0'}}>
                <div className="top-left">
                    <div className="navbar-header"  style={{width: '72%'},{backgroundColor: '#c4f5f0'}}>
                        <a className="navbar-brand" href="./"><h1>H2C Team</h1></a>
                        <a className="navbar-brand hidden" href="./"><img src="images/logo2.png" alt="Logo" /></a>
                       </div>
                </div>
                <div className="top-right" >
                    <div className="header-menu">
                        <div className="header-left">
                            <button className="search-trigger"><i className="fa fa-search" /></button>
                            <div className="form-inline">
                                <form className="search-form">
                                    <input className="form-control mr-sm-2" type="text" placeholder="Search ..." aria-label="Search" />
                                    <button className="search-close" type="submit"><i className="fa fa-close" /></button>
                                </form>
                            </div>
                            <div className="user-area dropdown float-right">
                                <a href="#" className="dropdown-toggle active" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    <img className="user-avatar rounded-circle" src="images/admin.jpg" alt="User Avatar" />
                                </a>
                                <div className="user-menu dropdown-menu">
                                    <a className="nav-link" href="#"><i className="fa fa- user" />My Profile</a>
                                    <a className="nav-link" href="#"><i className="fa fa- user" />Notifications <span className="count">13</span></a>
                                    <a className="nav-link" href="#"><i className="fa fa -cog" />Settings</a>
                                    <a className="nav-link" href="#"><i className="fa fa-power -off" />Logout</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                </header>

        );
    }
}

export default HeaderHome;