import React from 'react';
import HomePage from './pages/HomePage/HomePage';
import NotFoundPage from './pages/NotFoundPage/NotFoundPage';
import StudentPage from './pages/studentPage/StudentPage';

const routes=[
    {
        path: '/',
        exact: true,
        main: ()=> <HomePage/>
    },
    {
        path: '/student-page',
        exact: true,
        main: ()=> <StudentPage/>
    },
    {
        path: '',
        exact: false,
        main: ()=> <NotFoundPage/>
    }
];

export default routes;