export function LoginAc( userData) {
    let Url = 'https://a2b5475d.ngrok.io/api/auth/signin'
    return new Promise((resolve, reject) => {
        fetch(Url, {
            method: "POST",
            headers: {
                'Content-Type': 'application/json',
                Accept: 'application/json',
              },
            body:JSON.stringify(userData)
            
        })
            .then(response => response.json())
            .then(responseJson => {
                resolve(responseJson);
            })
            .catch((error)=>{
                reject(error);
            });
    }
    )
}