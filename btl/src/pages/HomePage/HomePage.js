import React, { Component } from 'react';
import Header from '../../components/login/header/Header';
import Flexslider from '../../components/login/contents/Flexslider';
import Course from '../../components/login/contents/Course';
import Review from '../../components/login/contents/Review';
import Showcase from '../../components/login/contents/Showcase';
import FeedBack from '../../components/login/contents/FeedBack';
import Footer from '../../components/login/footer/Footer';


class HomePage extends Component {
    render() {
        return (
            <div>
                <Header/>
                <Flexslider />
                <Course />
                <Review />
                <Showcase />
                <FeedBack />
                <Footer/>
            </div>
        );
    }
}

export default HomePage;