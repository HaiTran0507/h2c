import React, { Component } from 'react';
import { Redirect } from 'react-router-dom'
import HeaderHome from '../../components/home/header/HeaderHome'
import LeftContent from '../../components/home/contents/LeftContent';
import Course from '../../components/login/contents/Course';
import Review from '../../components/login/contents/Review';
import Showcase from '../../components/login/contents/Showcase';
import FeedBack from '../../components/login/contents/FeedBack';
import FooterHome from '../../components/home/footer/FooterHome';
class StudentPage extends Component {
    // constructor(props) {
    //     super(props);
    //     this.state = {
    //         redirect: false
    //     }
    //     this.logout = this.logout.bind(this);
    // }

    // componentWillMount() {
    //     if (sessionStorage.getItem("accessTokename")) {
    //         console.log(sessionStorage.getItem("accessTokename"));
    //         // this.setState({ redirect: true });

    //     }
    // }
    // logout() {
    //     sessionStorage.setItem("accessTokename", '')
    //     sessionStorage.clear();
    //     this.setState({ redirect: false });

    // }
    render() {
        // if (this.state.redirect) {
        //     return <Redirect to={'/'} />
        // }
        return (

            <div>
                <div id="right-panel" className="right-panel">
                    <HeaderHome />
                    <Course />
                    <Review />
                    <Showcase />
                    <FeedBack />
                    <FooterHome />
                </div>
                <LeftContent />
                
            </div>
        );
    }
}

export default StudentPage;