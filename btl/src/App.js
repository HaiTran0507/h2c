import React, { Component } from 'react';
import {  Route, BrowserRouter as Router } from 'react-router-dom';
import HomePage from './pages/HomePage/HomePage';
import StudentPage from './pages/studentPage/StudentPage';
class App extends Component {
  render() {
    return (
      <Router>
        <Route exact path="/" component={HomePage} />
        <Route path="/student-page" component={StudentPage} />
      </Router>
    );
  
  }
}

export default App;

